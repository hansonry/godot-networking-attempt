# Tags
<default> - Calls can only come from autority (server unless set otherwise)
authority - Calls can only comome from authority (like default case)
any_peer - Calls can come from anyone
call_local - Calls localy too.
call_remote - Only calls specific remote client (opposite of call_local)

# Functions

rpc(method_name) - Calls RPC on all peers and local
rpc_id(peer_id, method_name) - Calls RPC on specific peer

