extends Node2D

var _networking : Networking

# Called when the node enters the scene tree for the first time.
func _ready():
	_networking = $/root/Networking
	_networking.game_connect()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func get_player_nodes():
	return get_children()
