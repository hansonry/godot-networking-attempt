extends Node2D

var leIPAddress : LineEdit
var lePort : LineEdit
var networking : Networking

# Called when the node enters the scene tree for the first time.
func _ready():
	leIPAddress = $Panel/leIPAddress
	lePort = $Panel/lePort
	networking = $/root/Networking



func _on_btn_host_pressed():
	print("Host Pressed")
	#_setup_as_host(1234)
	networking.setup_as_host(1234)
	# TODO: Looks like we need a singleton to hold state before we transition.
	_change_scene()


func _on_btn_join_pressed():
	print("Join Pressed. IPAddress: ", leIPAddress.text, ", Port: ", lePort.text)
	#_setup_as_client(leIPAddress.text, lePort.text)
	networking.setup_as_client("localhost", 1234)
	_change_scene()


func _change_scene():
	get_tree().change_scene_to_file("res://arena.tscn")


