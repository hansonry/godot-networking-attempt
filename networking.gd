extends Node

var _is_host: bool = false
var _ip_address: String = "localhost"
var _port: int = 1234

var _multiplayer : MultiplayerAPI
var _arena : Node2D
# Called when the node enters the scene tree for the first time.
func _ready():
	_multiplayer = get_tree().get_multiplayer()
	_multiplayer.peer_connected.connect(_player_connected)
	_multiplayer.peer_disconnected.connect(_player_disconnected)
	_multiplayer.connected_to_server.connect(_connected_ok)
	_multiplayer.connection_failed.connect(_connected_fail)
	_multiplayer.server_disconnected.connect(_server_disconnected)

func setup_as_host(port):
	_is_host = true
	_port = port
	
func setup_as_client(ipAddress, port):
	_is_host = false
	_ip_address = ipAddress
	_port = port

func game_connect():
	_arena = $/root/Arena
	print("Arena: ", _arena)
	var peer = ENetMultiplayerPeer.new()
	if(_is_host):
		peer.create_server(_port, 10)
	else:
		peer.create_client(_ip_address, _port)
	_multiplayer.multiplayer_peer = peer
	
	# If your the server add your own self
	if _multiplayer.is_server():
		add_player(_multiplayer.get_unique_id(), true)
	
func _player_connected(id):
	print("Player Connected. Id: ", id)
	if _multiplayer.is_server():
		var server_id = _multiplayer.get_unique_id()
		for player_node in _arena.get_player_nodes():
			var player_id = player_node.net_id
			if player_id != server_id:
				rpc_id(player_id, "add_player", id, false)
				rpc_id(id, "add_player", player_id, false)
		add_player(id, false)
		rpc_id(id, "add_player", server_id, false)
		rpc_id(id, "add_player", id, true)
		
	# Called on both clients and server when a peer connects. Send my info to it.
	#rpc_id(id, "register_player", my_info)
	pass

func _player_disconnected(id):
	print("Player Disconnected. Id: ", id)
	#player_info.erase(id) # Erase player from info.
	pass

func _connected_ok():
	print("Client Connected Ok")
	pass # Only called on clients, not server. Will go unused; not useful here.

func _server_disconnected():
	print("Client Disconnected by server")
	pass # Server kicked us; show error and abort.

func _connected_fail():
	print("Client failed to connect to server")
	pass # Could not even connect to server; abort.


@rpc('authority')
func add_player(id: int, controlled: bool):
	var my_player = preload("res://player.tscn").instantiate()
	my_player.set_name(str(id))
	my_player.net_id = id
	my_player.controlled = controlled
	#my_player.set_network_master(id) # Will be explained later
	_arena.add_child(my_player)
	print("Added player for player id: ", id, ", controlled: ", controlled)
	

#remote func register_player(info):
#	# Get the id of the RPC sender.
#	var id = get_tree().get_rpc_sender_id()
#	# Store the info
#	player_info[id] = info


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
