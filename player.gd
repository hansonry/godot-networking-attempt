extends CharacterBody2D


const SPEED = 300.0

var sprite: Sprite2D

var controlled : bool = false
var net_id : int = 0

var _input_timer : float = 0
const _input_timeout : float = 0.100

var _update_timer : float = 0
const _update_timeout : float = 0.100

var _last_command_dx : float = 0
var _last_command_dy : float = 0

var _multiplayer: MultiplayerAPI

var _commanded_dx: float = 0
var _commanded_dy: float = 0

const FLOAT_EPSILON = 0.00001

static func compare_floats(a, b, epsilon = FLOAT_EPSILON):
	return abs(a - b) <= epsilon

func _ready():
	sprite = $Sprite2D
	_multiplayer = get_tree().get_multiplayer()


func _face_velocity():
	if velocity:
		var face = 0
		if velocity.x > 0:
			face = 1
		elif velocity.x < 0:
			face = 3
		elif velocity.y > 0:
			face = 2
		sprite.frame_coords.y = face
		
func _physics_process(delta):
	# Get the input direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	if _multiplayer.is_server():
		var dirX = _commanded_dx
		var dirY = _commanded_dy
		velocity.x = dirX * SPEED
		velocity.y = dirY * SPEED

		move_and_slide()
		_face_velocity()
		

@rpc('any_peer', 'call_local')
func set_player_command(dx :float, dy:float):
	var called_from_id = _multiplayer.get_remote_sender_id()
	if called_from_id == net_id:
		_commanded_dx = dx
		_commanded_dy = dy
	
func _check_for_input_change():
	var dx = Input.get_axis("move_left", "move_right")
	var dy = Input.get_axis("move_up",   "move_down")
	if (not (compare_floats(dx, _last_command_dx) and 
	compare_floats(dy, _last_command_dy))):
		_last_command_dx = dx
		_last_command_dy = dy
		rpc_id(1,"set_player_command", dx, dy)
@rpc('authority')
func update_pos_vel(pos :Vector2, vel: Vector2):
	velocity = vel
	position = pos
	_face_velocity()
	
func _send_update():
	rpc("update_pos_vel", position, velocity)
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if controlled:
		_input_timer += delta
		if _input_timer > _input_timeout:
			_input_timer -= _input_timeout
			_check_for_input_change()
	if _multiplayer.is_server():
		_update_timer += delta
		if _update_timer > _update_timeout:
			_update_timer -= delta
			_send_update()
